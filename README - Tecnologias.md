Referente as tecnologias utilizadas podemos destacar:
Para edição dos arquivos utilizei o Notepad++ e o Visual Studio Code, por simplicidade e familiariedade com os respectivos softwares.
Wampserver é uma aplicação que instala um ambiente de desenvolvimento web no Windows. Com ele você pode criar aplicações web com Apache2, PHP e banco de dados MySQL. 
Utilizei o banco de dados MySQL e o Workbench 8.0 da Oracle, para criação, edição e testes do banco de dados, posteriormente configurei o banco de dados remoto, para testar a aplicação na web.
Para os testes utilizei o Google chrome, Safari e Internet Explorer.