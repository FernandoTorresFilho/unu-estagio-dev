O projeto poderá ser testado diretamente no site: https://fernandinhoo15.000webhostapp.com/

Para realizar a execução do projeto na máquina local, primeiramente recomendo a instalação do Wampserver, os arquivos deverão ser baixados e alocados na pasta C:\wamp64\www
O arquivo conexão.php está configurado para um banco de dados gratuito e remoto (remotemysql.com), se desejar poderá alterar as configurações para um banco de dados local e realizar a execução do script, criando o banco de dados e as tabelas que serão utilizadas.
Em seguida, deverá ser inicializado o  Wampserver, abrir o navegador no endereço: localhost e abrir o projeto.
Para a primeira utilização, deverá cadastrar um usuário, após realizar o login, poderá adicicionar novos alunos, alterar, remover e realizar a busca pelos alunos.